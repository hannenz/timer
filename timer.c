#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>

#define VERSION "0.0.2"
/**
 * Very simple timer for CLI
 *
 * Usage
 *
 * timer [time]
 *
 * Where time is a string in [[hh:]mm:]ss format
 * If time is given timer works like a - well - timer, counting from the
 * specified time to zero
 * If no time is specified then it works like a stopwatch, simply counting
 * forward forever
 * The timer can be stopped in either mode with <Ctrl+C>
 */

#define O "\033[7m  \033[27m"
#define _ "  "

const char *digits[11][5] = {
	{
		O O O,
		O _ O,
		O _ O,
		O _ O,
		O O O
	},
	{
		_ _ O,
		_ _ O,
		_ _ O,
		_ _ O,
		_ _ O
	},
	{
		 O  O  O ,
		 _  _  O ,
		 O  O  O ,
		 O  _  _ ,
		 O  O  O 
	},
	{
		 O  O  O ,
		 _  _  O ,
		 O  O  O ,
		 _  _  O ,
		 O  O  O 
	},
	{
		 O  _  O ,
		 O  _  O ,
		 O  O  O ,
		 _  _  O ,
		 _  _  O 
	},
	{
		 O  O  O ,
		 O  _  _ ,
		 O  O  O ,
		 _  _  O ,
		 O  O  O 
	},
	{
		 O  _  _ ,
		 O  _  _ ,
		 O  O  O ,
		 O  _  O ,
		 O  O  O 
	},
	{
		 O  O  O ,
		 _  _  O ,
		 _  _  O ,
		 _  _  O ,
		 _  _  O 
	},
	{
		 O  O  O ,
		 O  _  O ,
		 O  O  O ,
		 O  _  O ,
		 O  O  O 
	},
	{
		 O  O  O ,
		 O  _  O ,
		 O  O  O ,
		 _  _  O ,
		 O  O  O 
	},
	{
		 _  _  _ ,
		 _  O  _ ,
		 _  _  _ ,
		 _  O  _ ,
		 _  _  _ 
	}
};


void usage(char **argv) {
	printf("Usage: %s [options]\n", argv[0]);
	printf("-q\t\tQuiet (behaves like sleep ;-))\n");
	printf("-h\t\tShow this help\n");
	printf("-b\t\tBig digits\n");
	printf("-v\t\tPrint version\n");
	printf("\n");
}


void print_clock_big(unsigned int count) {
	char string[8];
	int c, x, y;

	snprintf(string, sizeof(string), "%u:%02u:%02u", count / 3600, (count % 3600) / 60, count % 60);

	for (y = 0; y < 5; y++) {
		for (x = 0; x < strlen(string); x++) {
			c = (string[x] == ':') ? 10 : string[x] - '0';
			printf("%s ", digits[c][y]);
		}
		printf("\n");
	}
	// Cursor up 5x and carriage return
	printf("\033[A\033[A\033[A\033[A\033[A\r");
	fflush(stdout);
}


void print_clock(unsigned int count) {
	printf("\r%u:%02u:%02u", count / 3600, (count % 3600) / 60, count % 60);
	fflush(stdout);
}


int main(int argc, char **argv) {

	char *token;
	int count = 0;
	int dir = -1;
	int quiet = false;

	int option;

	void(*printer)(unsigned int) = &print_clock;

	// Parse options
	while ((option = getopt(argc, argv, "hqbv")) != -1) {
		switch (option) {
			case 'q':
				quiet = true;
				break;
			case 'h':
				usage(argv);
				return 0;
			case 'v':
				puts(VERSION);
				return 0;
			case 'b':
				printer = &print_clock_big;
				break;
			case '?':
			default:
				printf("Unknown option: -%c", option);
				break;
		}
	}

	/* printf("optind=%u, argument=%s\n", optind, argv[optind]); */
	/* return 0; */

	const char *arg = argv[optind];
	if (arg != NULL) {
		for (token = strtok((char*)arg, ":"); token != NULL; token = strtok(NULL, ":")) {
			count *= 60;
			count += atoi(token);
		}
	}
	else {
		dir = 1;
	}

	while (count > 0 || dir > 0) {
		if (!quiet) {
			(*printer)(count);
			/* print_clock(count); */
		}
		sleep (1);
		count += dir;
	}

	return 0;
}

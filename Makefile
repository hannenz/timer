PRG=timer
# CC=arm-linux-gnueabi-gcc
CC=gcc

SOURCES=timer.c

all: $(PRG)
$(PRG):	$(SOURCES)
	$(CC) -static -o $(PRG) $(SOURCES)

install: 
	install -m 755 $(PRG) /usr/local/bin/

clean:
	rm -f $(PRG)

.PHONY: all clean


Very simple timer for CLI

Usage

```
timer [time]
```

Where `time` is a string in `[[hh:]mm:]ss` format
If time is given timer works like - well - a timer, counting from the
specified time to zero.
If no time is specified then it works like a stopwatch, simply counting
forward forever
The timer can be stopped in either mode with <Ctrl+C>

If timer ends without being aborted, it will return 0, so it is possible to do something like this:

```
timer 10:00 && notify-send "Time is up!"
```

